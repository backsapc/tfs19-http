package tfs.scala.http.client

import com.softwaremill.sttp._
import tfs.scala.http.client.util.{HttpClientTest, Measure}

class SyncHttpClientTest extends HttpClientTest {
  implicit val backend: SttpBackend[Id, Nothing] = HttpURLConnectionBackend()

  behavior of "SyncHttpClient"

  it should s"send $iteration async requests" in Measure {
    (1 to iteration).foreach { index =>
      logger.info(s"[$index] Sending request")
      sendRequest()
      logger.info(s"[$index] Request completed")
    }
  }

  private def sendRequest(): Id[Response[String]] = {
    val request = sttp.get(uri"https://httpbin.org/delay/${timeout.toSeconds}")
    request.send()
  }
}